function ParamsD=Extract_Param_Dubins(param,A,TA,B,TB,Type_Dubins)
tta=(-pi:0.01:pi)';
if strcmp(Type_Dubins(1),'R'),
    NA=[TA(2);-TA(1)];
    sigA=-1;
else
    NA=[-TA(2);TA(1)];
    sigA=1;
end
CA=A'+param.r*NA;
xx=CA(1)+param.r*cos(tta);
yy=CA(2)+param.r*sin(tta);
plot(xx,yy,'w')
angA=atan2(A(2)-CA(2),A(1)-CA(1));
PA=CA+param.r*[cos(angA);sin(angA)];
plot(PA(1),PA(2),'sg','markersize',14)
ecartAngA=param.seg_param(1);
uA=angA+sigA*ecartAngA;
QA=CA+param.r*[cos(uA);sin(uA)];
plot(QA(1),QA(2),'pg','markersize',14)



if strcmp(Type_Dubins(3),'R'),
    NB=[TB(2);-TB(1)];
        sigB=1;
else
    NB=[-TB(2);TB(1)];
        sigB=-1;
end
CB=B'+param.r*NB;
xx=CB(1)+param.r*cos(tta);
yy=CB(2)+param.r*sin(tta);
plot(xx,yy,'w')
angB=atan2(B(2)-CB(2),B(1)-CB(1));
PB=CB+param.r*[cos(angB);sin(angB)];
plot(PB(1),PB(2),'sr','markersize',14)
ecartAngB=param.seg_param(3);
uB=angB+sigB*ecartAngB;
QB=CB+param.r*[cos(uB);sin(uB)];
plot(QB(1),QB(2),'pr','markersize',14)

if ~strcmp(Type_Dubins(2),'S'),
    NQA=[cos(uA);sin(uA)];
    CC=QA+param.r*NQA;
    xx=CC(1)+param.r*cos(tta);
    yy=CC(2)+param.r*sin(tta);
    plot(xx,yy,'w')
else
    CC=[NaN NaN]';
end

ParamsD.r=param.r;
ParamsD.seg_path=param.seg_param*param.r;
ParamsD.s=cumsum(ParamsD.seg_path/sum(ParamsD.seg_path));
ParamsD.Type=Type_Dubins;
ParamsD.Points=[A ;...
    QA';...
    QB';...
    B];
ParamsD.Cercle1=[CA'];
ParamsD.Midlle=[CC'];
ParamsD.Cercle2=[CB'];