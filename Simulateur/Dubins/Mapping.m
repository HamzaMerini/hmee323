function [map,xmax,ymax] = Mapping(x,y,Resolution,nmbrObst)
load exampleMaps.mat;
[xmax,ymax] = size(simpleMap);
map = robotics.BinaryOccupancyGrid(xmax,ymax,Resolution);
setOccupancy(map, [x y], ones(nmbrObst,1));

end

