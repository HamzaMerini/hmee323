function [map,xmax,ymax] = Mapping2
% Import Image
%filepath = fullfile(matlabroot,'examples','robotics','Map1.png');
image = imread('Map1.png');
% Convert to grayscale and then black and white image based on arbitrary threshold.
grayimage = rgb2gray(image);
bwimage = grayimage < 0.5;
% Use black and white image as matrix input for binary occupancy grid
map = robotics.BinaryOccupancyGrid(bwimage);
[xmax,ymax] = size(map);

for x=0:1:xmax
    for y=0:1:ymax
        if map(
occupancy==1

end

