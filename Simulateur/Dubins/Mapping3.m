function [X1,Y1,X2,Y2,xmax,ymax] = Mapping3(xmax,ymax)
R1 = input('Entrez le nombre dobstacles horizontals que vous voulez placez : ');
msgbox('Placer les obstacles');
pause(1);
xlim([0, xmax]);
ylim([0, ymax]);
title('Obstacles horizontals');
hold on
for i=1:1:R1
    [x1,y1]=ginput(1);
    P=[x1 y1 3 1.5];
    rectangle('Position',P);
    X1(i)=P(1);
    Y1(i)=P(2);
    hold on
end
R2 = input('Entrez le nombre dobstacles verticals que vous voulez placez : ');
title('Obstacles verticals');
for i=1:1:R2
    [x1,y1]=ginput(1);
    P=[x1 y1 1.5 3];
    rectangle('Position',P);
    X2(i)=P(1);
    Y2(i)=P(2);
    hold on
end

xlim([0, xmax]);
ylim([0, ymax]);
hold on
end

