function [Xpatch,Ypatch,x,y,TA] = Robot(theta,longueur,largeur,E)

%% Positionner le robot
msgbox('Cliquez pour positionner le robot');
pause(1);
title('Positionner le robot')
[x y] = ginput(1) ;
plot(x,y,'og')
hold on
P = [x y];
%% Direction du vecteur vitesse du robot
Xpatch=[x+longueur*cos(theta),x-(largeur/2),x+(largeur/2)];
Ypatch=[y+largeur*sin(theta),y,y];

msgbox('Dirigez le vecteur du robot');
pause(1);
title('Direction du robot')
[vecxR,vecyR]=ginput(1);
vecR =[vecxR,vecyR];

TA=vecR-P;
TA=TA/norm(TA);
line([x x+TA(1)*E],[y y+TA(2)*E],'color','g','linewidth',2,'linestyle','-');
hold on

end

