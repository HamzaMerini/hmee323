clear all 
close all 
clc 

%% constante
Tmax = 2;
dt = 0.1;
E=1;
stepsize=0.1;
Radius=1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% D�claration de la map et des obstacles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[X1,Y1,X2,Y2,xmax,ymax]=Mapping3(20,20);
hold on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Donn�es initiales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rob.theta = pi/2;
rob.longueur = 0.7;
rob.largeur = 0.7;
[Xpatch,Ypatch,rob.x,rob.y,TA]=Robot(rob.theta,rob.longueur,rob.largeur,E);
xc1=rob.x;yc1=rob.y;
xc2=rob.x+rob.longueur*1.5*cos(rob.theta);yc2=rob.y+rob.largeur*1.5*sin(rob.theta);
Xc=[xc1 xc2];
Yc=[yc1 yc2];

xc21=rob.x;yc21=rob.y;
xc22=rob.x+rob.longueur*cos(rob.theta-(pi/4));yc22=rob.y+rob.largeur*sin(rob.theta-(pi/4));
Xc2=[xc21 xc22];
Yc2=[yc21 yc22];

xc31=rob.x;yc31=rob.y;
xc32=rob.x+rob.longueur*cos(rob.theta+(pi/4));yc32=rob.y+rob.largeur*sin(rob.theta+(pi/4));
Xc3=[xc31 xc32];
Yc3=[yc31 yc32];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Position d�sir�e
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
msgbox('Position d�sir�e ?');
pause(1);
title('Position darriv�e')
[xd,yd]=ginput(1);
Pd=[xd,yd];
plot(xd,yd,'or')
hold on
    %% Vecteur d�sir�e
msgbox('Vecteur de la direction darriv�e ?');
pause(1);
title('Direction darriv�e')
[vecdx(1),vecdy(2)]=ginput(1);
vecd=[vecdx(1),vecdy(2)];
TB=vecd-Pd;
TB=TB/norm(TB);
line([Pd(1) Pd(1)+TB(1)*E],[Pd(2) Pd(2)+TB(2)*E],'color','r','linewidth',2,'linestyle','-')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialisation graphique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
title('Simulation suivi de trajectoire et �vitement dobstacles')
rob.ptr=patch(Xpatch,Ypatch,'black');
C1=line(Xc,Yc,'color','g','linewidth',1);
C2=line(Xc2,Yc2,'color','g','linewidth',1);
C3=line(Xc3,Yc3,'color','g','linewidth',1);
etat = [rob.x;rob.y;rob.theta];
set(C1,'xdata',[rob.x rob.x+rob.longueur*1.5*cos(rob.theta)],'ydata',[rob.y rob.y+rob.largeur*1.5*sin(rob.theta)]);
set(C2,'xdata',[rob.x rob.x+rob.longueur*cos(rob.theta-(pi/4))],'ydata',[rob.y rob.y+rob.largeur*sin(rob.theta-(pi/4))]);
set(C3,'xdata',[rob.x rob.x+rob.longueur*cos(rob.theta+(pi/4))],'ydata',[rob.y rob.y+rob.largeur*sin(rob.theta+(pi/4))]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Boucle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T=1;
P=[rob.x,rob.y];
Traj=zeros(3,Tmax/dt+1);
xc2=rob.x+rob.longueur*1.5*cos(rob.theta);yc2=rob.y+rob.largeur*1.5*sin(rob.theta);
xc22=rob.x+rob.longueur*cos(rob.theta-(pi/4));yc22=rob.y+rob.largeur*sin(rob.theta-(pi/4));
xc32=rob.x+rob.longueur*cos(rob.theta+(pi/4));yc32=rob.y+rob.largeur*sin(rob.theta+(pi/4));
xdetect=[xc2 xc22 xc32];
ydetect=[yc2 yc22 yc32];
[v,w,obst]=Detect(X1,Y1,X2,Y2,xdetect,ydetect,xmax,ymax);

j=0;
destination = arrivee(rob.x,rob.y,xd,yd);
while(destination==0)
while(obst==0 && destination==0)
i=1;
p1=[P';atan2(TA(2),TA(1))];
p2=[Pd';atan2(TB(2),TB(1))];
    %% Determination des courbes de dubins
tic
[path,param] = dubins_curve(p1,p2,Radius, stepsize,'true');
toc
    %% Exploitation des courbes de dubins
plot(path(:,1),path(:,2),':b')
hold on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Boucle de suivi de trajectoire
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while(i<length(path) && obst==0)
    rob.x = path(i,1);
    rob.y = path(i,2);
    rob.theta= path(i,3);
    %% Dessin de trajectoire
    [Traj,T]=MemoryTraj(T,rob.x,rob.y,rob.theta,Traj);
    %% Initialisation du mouvement du robot
    set(rob.ptr,'xdata',[rob.x+rob.longueur*cos(rob.theta);rob.x-(rob.largeur/2)*cos((pi/2)-rob.theta);rob.x+(rob.largeur/2)*cos((pi/2)-rob.theta)],'ydata',[rob.y+rob.largeur*sin(rob.theta);rob.y+(rob.largeur/2)*sin((pi/2)-rob.theta);rob.y-(rob.largeur/2)*sin((pi/2)-rob.theta)]);
    set(C1,'xdata',[rob.x rob.x+rob.longueur*1.5*cos(rob.theta)],'ydata',[rob.y rob.y+rob.largeur*1.5*sin(rob.theta)]);
    set(C2,'xdata',[rob.x rob.x+rob.longueur*cos(rob.theta-(pi/4))],'ydata',[rob.y rob.y+rob.largeur*sin(rob.theta-(pi/4))]);
    set(C3,'xdata',[rob.x rob.x+rob.longueur*cos(rob.theta+(pi/4))],'ydata',[rob.y rob.y+rob.largeur*sin(rob.theta+(pi/4))]);
    drawnow
    i=i+1;
    P=[rob.x,rob.y];
    xc2=rob.x+rob.longueur*1.5*cos(rob.theta);yc2=rob.y+rob.largeur*1.5*sin(rob.theta);
    xc22=rob.x+rob.longueur*cos(rob.theta-(pi/4));yc22=rob.y+rob.largeur*sin(rob.theta-(pi/4));
    xc32=rob.x+rob.longueur*cos(rob.theta+(pi/4));yc32=rob.y+rob.largeur*sin(rob.theta+(pi/4));
    xdetect=[xc2 xc22 xc32];
    ydetect=[yc2 yc22 yc32];
    [~,~,obst]=Detect(X1,Y1,X2,Y2,xdetect,ydetect,xmax,ymax);
    T=T+1;
    pause(0.1)
end
    %% Reinitialisation du vecteur directeur
    vecxR=Pd(1);
    vecyR=Pd(2);
    vecR =[vecxR,vecyR];
    TA=vecR-P;
    TA=TA/norm(TA);
    line([rob.x rob.x+TA(1)*E],[rob.y rob.y+TA(2)*E],'color','g','linewidth',2,'linestyle','-');
    hold on
    destination = arrivee(rob.x,rob.y,xd,yd);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Boucle d'evitement d'obstacles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(obst==1)
    etat = [rob.x;rob.y;rob.theta];
    while(j<Tmax)
    rob.x = etat(1);
    rob.y = etat(2);
    rob.theta= etat(3);
    %% Dessin de trajectoire
    [Traj,T]=MemoryTraj(T,rob.x,rob.y,rob.theta,Traj);
    %% Initialisation du mouvement du robot
    set(rob.ptr,'xdata',[rob.x+rob.longueur*cos(rob.theta);rob.x-(rob.largeur/2)*cos((pi/2)-rob.theta);rob.x+(rob.largeur/2)*cos((pi/2)-rob.theta)],'ydata',[rob.y+rob.largeur*sin(rob.theta);rob.y+(rob.largeur/2)*sin((pi/2)-rob.theta);rob.y-(rob.largeur/2)*sin((pi/2)-rob.theta)]);
    set(C1,'xdata',[rob.x rob.x+rob.longueur*1.5*cos(rob.theta)],'ydata',[rob.y rob.y+rob.largeur*1.5*sin(rob.theta)]);
    set(C2,'xdata',[rob.x rob.x+rob.longueur*cos(rob.theta-(pi/4))],'ydata',[rob.y rob.y+rob.largeur*sin(rob.theta-(pi/4))]);
    set(C3,'xdata',[rob.x rob.x+rob.longueur*cos(rob.theta+(pi/4))],'ydata',[rob.y rob.y+rob.largeur*sin(rob.theta+(pi/4))]);
    drawnow
    pause(0.1)
    %% D�tection d'obstacles
    xc2=rob.x+rob.longueur*1.5*cos(rob.theta);yc2=rob.y+rob.largeur*1.5*sin(rob.theta);
    xc22=rob.x+rob.longueur*cos(rob.theta-(pi/4));yc22=rob.y+rob.largeur*sin(rob.theta-(pi/4));
    xc32=rob.x+rob.longueur*cos(rob.theta+(pi/4));yc32=rob.y+rob.largeur*sin(rob.theta+(pi/4));
    xdetect=[xc2 xc22 xc32];
    ydetect=[yc2 yc22 yc32];
    [v,w,~]=Detect(X1,Y1,X2,Y2,xdetect,ydetect,xmax,ymax);
    commande=[v,w]';
    [t45,x45]=ode45(@(t,etat)modelrobot(t,etat,commande,rob),[0 dt],etat);
    L45=length(t45);
    etat=x45(L45,:)';
    j=j+dt;
    T=T+1;
    end    
    P=[rob.x,rob.y];
end
    %% Test d'obstacle et d'arriv�e � destination 
    j=0;
    [v,w,obst]=Detect(X1,Y1,X2,Y2,xdetect,ydetect,xmax,ymax);
    destination = arrivee(rob.x,rob.y,xd,yd);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Dessin Trajectoire figure 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(2)
for k=1:1:T-3
    plot(Traj(1,k),Traj(2,k),'.r')
    hold on
end
title('Trajectory of the Robot')
xlabel('X[meters]')
ylabel('Y[meters]')
xlim([0 xmax]);
ylim([0 ymax]);
