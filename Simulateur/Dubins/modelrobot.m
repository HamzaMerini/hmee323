function[etatdot]=modelrobot(t,etat,commande,rob)
etatdot=zeros(3,1);
etatdot(1)=commande(1)*cos(etat(3));
etatdot(2)=commande(1)*sin(etat(3));
etatdot(3)=commande(2);
end