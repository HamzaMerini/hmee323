clear all
close all
clc

d=5;

psi=0;
Ma=[cos(psi) -cos(psi) -cos(psi) cos(psi);
    sin(psi) sin(psi) -sin(psi) -sin(psi);
      -d        d        -d          d  ];
  
Pos_R=[0 0 0]';
Nu=[1;1;1];
dt=0.01;
cpt=1;
for t=0:dt:10
    Mc=[cos(Pos_R(3)) -sin(Pos_R(3)) 0;
        sin(Pos_R(3))  cos(Pos_R(3)) 0;
             0          0        1];
    
    ox=-sin(Pos_R(3));
    oy=cos(Pos_R(3));
    on=0;
    
    Xu=Pos_R(1);
    Yv=Pos_R(2);
    Nr=Pos_R(3);
    
    Xud=1;
    Yvd=1;
    Nrd=1;
    
    cpt=cpt+1;
    Actionneurs=[1,1,1,1]';
    F_B=Ma*Actionneurs;
    
    Dot_Nu(1)=(F_B(1)-dot(dot(Xu,Nu(1)),abs(Nu(1)))-ox)/Xud;
    Dot_Nu(2)=(F_B(2)-dot(dot(Yv,Nu(2)),abs(Nu(2)))-oy)/Yvd;
    Dot_Nu(3)=(F_B(3)-dot(dot(Nr,Nu(3)),abs(Nu(3)))-on)/Nrd;
    
    Dot_Dot_Eta=Mc*Dot_Nu;
    
    Dot_Eta=Dot_Eta+Dot_Dot_Eta*dt;
    Pos_R=Pos_R+Dot_Eta*dt;
    Pos_R_Stock(:,cpt)=Pos_R;
end


figure
subplot(2,1,1)
plot(Pos_R_Stock(1,:),Pos_R_Stock(2,:),'LineWidth',3)%trajectoire plan x y
grid on
title('Trajectoire Plan X Y')
subplot(2,1,2)
plot(time,Pos_R_Stock(3,:),'LineWidth',3)%angle orientation
grid on
title('Angle d''orientation') 


